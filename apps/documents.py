from django_elasticsearch_dsl import Document, Index, fields
from django_elasticsearch_dsl.registries import registry
from django_elasticsearch_dsl_drf.compat import StringField

from apps.models import Product, Category


@registry.register_document
class ProductDocument(Document):
    name = StringField(fields={
        'raw': StringField(analyzer='keyword'),
        'suggest': fields.CompletionField(),
    })

    category = fields.ObjectField(properties={
        'name': fields.TextField()
    })

    class Index:
        name = 'products'
        settings = {
            'number_of_shards': 1,
            'number_of_replicas': 0,
        }

    class Django:
        model = Product
        fields = [
            'description'
        ]
        related_models = [Category]  # Optional: to ensure the Car will be re-saved when Manufacturer or Ad is updated

    def get_queryset(self):
        """Not mandatory but to improve performance we can select related in one sql request"""
        return super(ProductDocument, self).get_queryset().select_related('category')
